package helpers

import (
	"context"
	"encoding/json"
	"log"
	"testing"
	"time"

	"gitlab.com/pantacor/pantahub-base/accounts"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/go-resty/resty"
	"gopkg.in/mgo.v2/bson"
)

// Register : Register user account
func Register(
	t *testing.T,
	email string,
	password string,
	nick string,
) (
	map[string]interface{},
	*resty.Response,
) {
	responseData := map[string]interface{}{}
	APIEndPoint := BaseAPIUrl + "/auth/accounts"
	res, err := resty.R().SetBody(map[string]string{
		"email":    email,
		"password": password,
		"nick":     nick,
	}).Post(APIEndPoint)

	if err != nil {
		t.Errorf("internal error calling test server " + err.Error())
		t.Fail()

	}
	err = json.Unmarshal(res.Body(), &responseData)
	if err != nil {
		t.Errorf(err.Error())
		t.Fail()
	}
	return responseData, res
}

// Login : Login user
func Login(
	t *testing.T,
	username string,
	password string,
) (
	map[string]interface{},
	*resty.Response,
) {
	response := map[string]interface{}{}
	APIEndPoint := BaseAPIUrl + "/auth/login"
	res, err := resty.R().SetBody(map[string]string{
		"username": username,
		"password": password,
	}).Post(APIEndPoint)

	if err != nil {
		t.Errorf("internal error calling test server " + err.Error())
		t.Fail()
	}
	err = json.Unmarshal(res.Body(), &response)
	if err != nil {
		t.Errorf(err.Error())
		t.Fail()
	}
	_, ok := response["token"].(string)
	if ok {
		UTOKEN = response["token"].(string)
	}
	return response, res
}

// VerifyUserAccount : Verify User Account
func VerifyUserAccount(
	t *testing.T,
	id string,
	challenge string,
) (
	map[string]interface{},
	*resty.Response,
) {
	responseData := map[string]interface{}{}
	APIEndPoint := BaseAPIUrl + "/auth/verify?id=" + id + "&challenge=" + challenge
	res, err := resty.R().Get(APIEndPoint)

	if err != nil {
		t.Errorf("internal error calling test server " + err.Error())
		t.Fail()
	}
	err = json.Unmarshal(res.Body(), &responseData)
	if err != nil {
		t.Errorf(err.Error())
		t.Fail()
	}
	return responseData, res
}

// GetUser : Get User Object
func GetUser(t *testing.T, email string, mongoDb *mongo.Database) accounts.Account {
	//	account := map[string]interface{}{}
	account := accounts.Account{}
	collection := mongoDb.Collection("pantahub_accounts")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err := collection.FindOne(ctx,
		bson.M{
			"email": email,
		}).Decode(&account)
	if err != nil {
		log.Print("Inside Error:")
		log.Print(err.Error())
		t.Errorf("Error fetching user record: " + err.Error())
	}
	return account
}

// DeleteAllUserAccounts : Delete All User Accounts
func DeleteAllUserAccounts(t *testing.T, mongoDb *mongo.Database) bool {
	collection := mongoDb.Collection("pantahub_accounts")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err := collection.DeleteMany(ctx, bson.M{})
	if err != nil {
		t.Errorf("Error on Removing: " + err.Error())
		t.Fail()
		return false
	}
	return true
}

// RefreshToken : Refresh Token
func RefreshToken(t *testing.T, token string) (map[string]interface{}, *resty.Response) {
	response := map[string]interface{}{}
	APIEndPoint := BaseAPIUrl + "/auth/login"
	res, err := resty.R().
		SetHeader("Authorization", "Bearer "+token).
		Get(APIEndPoint)

	if err != nil {
		t.Errorf("internal error calling test server " + err.Error())
		t.Fail()
	}
	err = json.Unmarshal(res.Body(), &response)
	if err != nil {
		t.Errorf(err.Error())
		t.Fail()
	}
	_, ok := response["token"].(string)
	if ok {
		UTOKEN = response["token"].(string)
	}
	return response, res
}
